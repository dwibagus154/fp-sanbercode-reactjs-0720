import React from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './Pages/login';
import Register from './Pages/register';
import Main from './layouts/Main';
import { UserProvider } from "./context/UserContext"


function App() {
  return (
    <>
      <UserProvider>
        <Main />
      </UserProvider>
    </>
  );
}

export default App;
