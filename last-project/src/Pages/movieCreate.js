import React, { useState, useEffect } from "react"
import axios from "axios"
import { Container } from '@material-ui/core';

const MovieCreate = () => {

    const [movies, setMovies] = useState(null)
    const [statusForm, setStatusForm] = useState("create")
    const [selectedId, setSelectedId] = useState(0)

    useEffect(() => {
        if (movies === null) {
            axios.get(`https://backendexample.sanbersy.com/api/movies`)
                .then(res => {
                    setMovies(res.data.map(el => {
                        return {
                            id: el.id,
                            title: el.title,
                            description: el.description,
                            year: el.year,
                            duration: el.duration,
                            genre: el.genre,
                            rating: el.rating,
                            review: el.review,
                            image_url: el.image_url
                        }
                    }))
                })
        }
    }, [movies])

    const [input, setInput] = useState({
        title: "",
        description: "",
        year: "",
        duration: 0,
        genre: "",
        rating: 0,
        review: "",
        image_url: ""
    })

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "title":
                {
                    setInput({ ...input, title: event.target.value });
                    break
                }
            case "description":
                {
                    setInput({ ...input, description: event.target.value });
                    break
                }
            case "year":
                {
                    setInput({ ...input, year: event.target.value });
                    break
                }
            case "duration":
                {
                    setInput({ ...input, duration: event.target.value });
                    break
                }
            case "genre":
                {
                    setInput({ ...input, genre: event.target.value });
                    break
                }
            case "rating":
                {
                    setInput({ ...input, rating: event.target.value });
                    break
                }
            case "review":
                {
                    setInput({ ...input, review: event.target.value });
                    break
                }
            case "image_url":
                {
                    setInput({ ...input, image_url: event.target.value });
                    break
                }
            default:
                { break; }
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        // let title = title
        // let description = description
        // let year = year
        // let duration = duration
        // let genre = genre
        // let rating = rating

        if (statusForm === "create") {
            axios.post(`https://backendexample.sanbersy.com/api/movies`, {
                title: input.title, description: input.description, year: input.year,
                duration: input.duration, genre: input.genre, rating: input.rating, review: input.review, image_url: input.image_url
            })
                .then(res => {
                    setMovies([
                        ...movies,
                        {
                            id: res.data.id,
                            title: input.title, description: input.description, year: input.year,
                            duration: input.duration, genre: input.genre, rating: input.rating, review: input.review, image_url: input.image_url
                        }])
                })

        } else if (statusForm === "edit") {
            axios.put(`https://backendexample.sanbersy.com/api/movies/${selectedId}`, {
                title: input.title, description: input.description, year: input.year,
                duration: input.duration, genre: input.genre, rating: input.rating, review: input.review, image_url: input.image_url
            })
                .then(() => {
                    let dataFilm = movies.find(el => el.id === selectedId)
                    dataFilm.title = input.title
                    dataFilm.description = input.description
                    dataFilm.year = input.year
                    dataFilm.duration = input.duration
                    dataFilm.genre = input.genre
                    dataFilm.rating = input.rating
                    dataFilm.review = input.review
                    dataFilm.image_url = input.image_url
                    setMovies([...movies])
                })
        }

        setStatusForm("create")
        setSelectedId(0)
        setInput({
            title: "",
            description: "",
            year: "",
            duration: 0,
            genre: "",
            rating: 0,
            review: "",
            image_url: ""
        })
        setTimeout(() => {
            window.location = "./"
        }, 500);
    }

    return (
        <>
            <h1 style={{ textAlign: "center", marginTop: 100, color: "black" }}>Create Movies</h1>
            <Container maxWidth="sm" style={{ color: "black" }}>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label style={{ float: "left" }}>
                            Title:
                        </label>
                        <input style={{ float: "right" }} type="text" name="title" value={input.title} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div>
                        <label style={{ float: "left" }}>
                            Description:
                        </label>
                        <textarea style={{ float: "right" }} type="text" name="description" value={input.description} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            Year:
                        </label>
                        <input style={{ float: "right" }} type="text" name="year" value={input.year} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            Duration:
                        </label>
                        <input style={{ float: "right" }} type="number" name="duration" value={input.duration} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            genre:
                    </label>
                        <input style={{ float: "right" }} type="text" name="genre" value={input.genre} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            Rating:
                    </label>
                        <input style={{ float: "right" }} type="number" name="release" value={input.number} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div>
                        <label style={{ float: "left" }}>
                            Review:
                        </label>
                        <textarea style={{ float: "right" }} type="text" name="review" value={input.review} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            URL Image:
                        </label>
                        <input style={{ float: "right" }} type="text" name="image_url" value={input.image_url} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <button>submit</button>
                </form>
            </Container>
        </>
    )
}
export default MovieCreate;