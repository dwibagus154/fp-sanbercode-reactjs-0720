import React, { useState, useEffect } from "react"
import axios from "axios"
import { Container } from '@material-ui/core';

const GameCreate = () => {

    const [games, setGames] = useState(null)
    const [statusForm, setStatusForm] = useState("create")
    const [selectedId, setSelectedId] = useState(0)

    useEffect(() => {
        if (games === null) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
                .then(res => {
                    setGames(res.data.map(el => {
                        return {
                            id: el.id,
                            name: el.name,
                            genre: el.genre,
                            singlePlayer: el.singlePlayer,
                            multiplayer: el.multiplayer,
                            platform: el.platform,
                            release: el.release,
                            image_url: el.image_url
                        }
                    }))
                })
        }
    }, [games])

    const [input, setInput] = useState({
        name: "",
        genre: "",
        singlePlayer: 0,
        multiplayer: 0,
        platform: "",
        release: "",
        image_url: ""
    })

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "name":
                {
                    setInput({ ...input, name: event.target.value });
                    break
                }
            case "genre":
                {
                    setInput({ ...input, genre: event.target.value });
                    break
                }
            case "singlePlayer":
                {
                    setInput({ ...input, singlePlayer: event.target.value });
                    break
                }
            case "multiplayer":
                {
                    setInput({ ...input, multiplayer: event.target.value });
                    break
                }
            case "platform":
                {
                    setInput({ ...input, platform: event.target.value });
                    break
                }
            case "release":
                {
                    setInput({ ...input, release: event.target.value });
                    break
                }
            case "image_url":
                {
                    setInput({ ...input, image_url: event.target.value });
                    break
                }
            default:
                { break; }
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        // let title = title
        // let description = description
        // let year = year
        // let duration = duration
        // let genre = genre
        // let rating = rating

        if (statusForm === "create") {
            axios.post(`https://backendexample.sanbersy.com/api/games`, {
                name: input.name, genre: input.genre, singlePlayer: input.singlePlayer,
                multiplayer: input.multiplayer, platform: input.platform, release: input.release, image_url: input.image_url
            })
                .then(res => {
                    setGames([
                        ...games,
                        {
                            id: res.data.id,
                            name: input.name, genre: input.genre, singlePlayer: input.singlePlayer,
                            multiplayer: input.multiplayer, platform: input.platform, release: input.release, image_url: input.image_url
                        }])
                })

        } else if (statusForm === "edit") {
            axios.put(`https://backendexample.sanbersy.com/api/games/${selectedId}`, {
                name: input.name, genre: input.genre, singlePlayer: input.singlePlayer,
                multiplayer: input.multiplayer, platform: input.platform, release: input.release, image_url: input.image_url
            })
                .then(() => {
                    let dataGame = games.find(el => el.id === selectedId)
                    dataGame.name = input.name
                    dataGame.genre = input.genre
                    dataGame.singlePlayer = input.singlePlayer
                    dataGame.multiplayer = input.multiplayer
                    dataGame.platform = input.platform
                    dataGame.release = input.release
                    dataGame.image_url = input.image_url
                    setGames([...games])
                })
        }

        setStatusForm("create")
        setSelectedId(0)
        setInput({
            name: "",
            genre: "",
            singlePlayer: 0,
            multiplayer: 0,
            platform: "",
            release: "",
            image_url: ""
        })
        setTimeout(() => {
            window.location = "./"
        }, 500);
    }

    return (
        <>
            <h1 style={{ textAlign: "center", marginTop: 100, color: "black" }}>Create Games</h1>
            <Container maxWidth="sm" style={{ color: "black" }}>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label style={{ float: "left" }}>
                            Name:
                    </label>
                        <input style={{ float: "right" }} type="text" name="name" value={input.name} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div>
                        <label style={{ float: "left" }}>
                            Genre:
                    </label>
                        <input style={{ float: "right" }} type="text" name="genre" value={input.genre} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            Single Player:
                        </label>
                        <input style={{ float: "right" }} type="number" max={1} min={0} name="singlePlayer" value={input.singlePlayer} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            Multi Player:
                        </label>
                        <input style={{ float: "right" }} type="number" name="multiplayer" value={input.multiplayer} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            Platform:
                    </label>
                        <input style={{ float: "right" }} type="text" name="platform" value={input.platform} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            Release:
                    </label>
                        <input style={{ float: "right" }} type="text" name="release" value={input.release} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <div style={{ marginTop: "20px" }}>
                        <label style={{ float: "left" }}>
                            URL Image:
                    </label>
                        <input style={{ float: "right" }} type="text" name="image_url" value={input.image_url} onChange={handleChange} />
                        <br />
                        <br />
                    </div>
                    <button>submit</button>
                </form>
            </Container>
        </>
    )
}
export default GameCreate;