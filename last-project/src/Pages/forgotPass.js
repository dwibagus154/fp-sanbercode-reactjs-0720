import React, { useContext, useState, useEffect } from "react"
import axios from "axios"
import { UserContext } from "../context/UserContext"
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
      </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Forgot() {
    const classes = useStyles();

    const [, setUser] = useContext(UserContext)
    const [input, setInput] = useState({ username: "", password: "", confirm: "" })
    const [selectedId, setSelectedId] = useState(0)
    const [people, setPeople] = useState(null)

    useEffect(() => {
        if (people === null) {
            axios.get(`https://backendexample.sanbersy.com/api/users`)
                .then(res => {
                    setPeople(res.data.map(el => {
                        return {
                            id: el.id,
                            created_at: el.created_at,
                            update_at: el.update_at,
                            username: el.username,
                            password: el.password,


                        }
                    }))
                })
        }
    }, [people])

    const handleSubmit = (event) => {
        var date = new Date();
        console.log(date);


        if (people === null) {
            axios.get(`https://backendexample.sanbersy.com/api/users`)
                .then(res => {
                    setPeople(res.data.map(el => {
                        if (input.username === el.username) {
                            setSelectedId(el.id)
                            console.log(selectedId)
                        }
                    }))
                })
        }
        // console.log(selectedId)

        event.preventDefault()
        if (input.password === input.confirm) {
            axios.put(`https://backendexample.sanbersy.com/api/users/${selectedId}`, {
                created_at: date,
                update_at: date,
                username: input.username,
                password: input.password
            })
                .then(res => {
                    let single = people.find(el => el.id === selectedId)
                    single.password = input.password
                    setPeople([...people])
                })
            setTimeout(() => {
                window.location = "./"
            }, 300);
        } else {
            alert("your data didn't match")
        }
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        switch (name) {
            case "username": {
                setInput({ ...input, username: value })
                break;
            }
            case "password": {
                setInput({ ...input, password: value })
                break;
            }
            case "confirm": {
                setInput({ ...input, confirm: value })
                break;
            }
            default: { break; }
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar />
                <Typography component="h1" variant="h5">
                    Forgot Password
                </Typography>
                <form className={classes.form} noValidate onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                                id="username"
                                label="Username"
                                name="username"
                                autoComplete="username"
                                value={input.username}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="New Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                value={input.password}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                                name="confirm"
                                label="Confirm Password"
                                type="password"
                                id="confirm"
                                autoComplete="current-password2"
                                value={input.confirm}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Create New Password
                     </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link href="#" variant="body2">
                                Already have an account? Sign in
              </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
}