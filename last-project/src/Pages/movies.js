import React, { useState, useEffect } from "react"
import axios from "axios"

const Movies = () => {
    const [movies, setMovies] = useState(null)

    useEffect(() => {
        if (movies === null) {
            axios.get(`https://backendexample.sanbersy.com/api/movies`)
                .then(res => {
                    setMovies(res.data.map(el => {
                        return {
                            id: el.id,
                            title: el.title,
                            description: el.description,
                            year: el.year,
                            duration: el.duration,
                            genre: el.genre,
                            rating: el.rating,
                            review: el.review,
                            image_url: el.image_url
                        }
                    }))
                })
        }
    }, [movies])


    const handleDelete = (event) => {
        let idDaftarMovies = parseInt(event.target.value)

        let newdaftarMovies = movies.filter(el => el.id != idDaftarMovies)

        axios.delete(`https://backendexample.sanbersy.com/api/movies/${idDaftarMovies}`)
            .then(res => {
                console.log(res)
            })

        setMovies([...newdaftarMovies])
        setTimeout(() => {
            window.location = "./"
        }, 300);

    }

    return (
        <>
            <h1 style={{ textAlign: "center", marginTop: 150, color: "black" }}>Daftar Movies</h1>
            <table style={{ borderCollapse: "collapse", width: "50%", margin: "auto", color: "black" }}>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Year</th>
                        <th>Duration</th>
                        <th>Genre</th>
                        <th>Rating</th>
                        <th>review</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    {
                        movies !== null && movies.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{item.title}</td>
                                    <td>{item.description}</td>
                                    <td>{item.year}</td>
                                    <td>{item.duration}</td>
                                    <td>{item.genre}</td>
                                    <td>{item.rating}</td>
                                    <td>{item.review}</td>
                                    <td>
                                        <img src={item.image_url} alt={item.title} width="50" />
                                    </td>
                                    <td>
                                        <button onClick={handleDelete} value={item.id}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </>
    )
}

export default Movies