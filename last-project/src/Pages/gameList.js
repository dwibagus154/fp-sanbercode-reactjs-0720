import React, { Component } from "react"
import axios from "axios"


class GameList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            games: []
        }
    }

    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/games`)
            .then(res => {
                let games = res.data.map(el => {
                    return {
                        id: el.id,
                        name: el.name,
                        genre: el.genre,
                        singlePlayer: el.singlePlayer,
                        multiplayer: el.multiplayer,
                        platform: el.platform,
                        release: el.release,
                        image_url: el.image_url
                    }
                })
                this.setState({ games })
            })
    }


    render() {
        return (
            <>
                <h1 style={{ color: "black" }}>Daftar Games</h1>
                <div id="article-list" style={{ color: "black" }}>
                    {
                        this.state.games.map((item) => {
                            return (
                                <div>
                                    <h3>{item.name}</h3>
                                    <strong>Platform {item.platform}</strong><br />
                                    <strong>Release: {item.release}</strong><br />
                                    <strong>genre: {item.genre}</strong>
                                    <p>
                                        <strong>Image</strong>:
                                        <img src={item.image_url} alt={item.name} width="100px" />
                                    </p>
                                    <a><button>Detail</button></a>
                                    <hr />
                                </div>
                            )
                        })
                    }
                </div>
            </>
        )
    }
}

export default GameList