import React, { useState, useEffect } from "react"
import axios from "axios"

const Game = () => {
    const [games, setGames] = useState(null)
    const [search, setSearch] = useState("");
    const [newgames, setNewgames] = useState(null)
    useEffect(() => {
        if (games === null) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
                .then(res => {
                    setGames(res.data.map(el => {
                        // if (search === "") {
                        return {
                            id: el.id,
                            name: el.name,
                            genre: el.genre,
                            singlePlayer: el.singlePlayer,
                            multiplayer: el.multiplayer,
                            platform: el.platform,
                            release: el.release,
                            image_url: el.image_url
                        }
                        // } else if (el.name == search) {
                        //     return {
                        //         id: el.id,
                        //         name: el.name,
                        //         genre: el.genre,
                        //         singlePlayer: el.singlePlayer,
                        //         multiplayer: el.multiplayer,
                        //         platform: el.platform,
                        //         release: el.release,
                        //         image_url: el.image_url
                        //     }
                        // }

                    }))
                })
        }
    }, [games])

    const handleDelete = (event) => {
        let idDaftarGames = parseInt(event.target.value)

        let newdaftarGames = games.filter(el => el.id != idDaftarGames)

        axios.delete(`https://backendexample.sanbersy.com/api/games/${idDaftarGames}`)
            .then(res => {
                console.log(res)
            })

        setGames([...newdaftarGames])
        setTimeout(() => {
            window.location = "./"
        }, 300);

    }

    const handleChange = (event) => {
        event.preventDefault()
        setSearch(event.target.value);
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(search)
        if (newgames === null) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
                .then(res => {
                    setNewgames(res.data.map(el => {
                        if (search === el.name) {
                            return {
                                id: el.id,
                                name: el.name,
                                genre: el.genre,
                                singlePlayer: el.singlePlayer,
                                multiplayer: el.multiplayer,
                                platform: el.platform,
                                release: el.release,
                                image_url: el.image_url
                            }
                        }
                        else {
                            return null
                        }
                    }))
                })
        }

        setGames(newgames);
        setNewgames(null);
        console.log(games);
        console.log(newgames)
    }

    return (
        <>
            <h1 style={{ textAlign: "center", marginTop: 100, color: "black" }}>Daftar Games</h1>
            <form style={{ marginLeft: "350px" }} onSubmit={handleSubmit}>
                <input type="text" placeholder="Search" onChange={handleChange} />
                <button>Search</button>
            </form>
            <table style={{ borderCollapse: "collapse", width: "50%", margin: "auto", color: "black" }}>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Genre</th>
                        <th>SinglePlayer</th>
                        <th>Multiplayer</th>
                        <th>Platform</th>
                        <th>Release</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    {
                        games !== null && games.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.genre}</td>
                                    <td>{item.singlePlayer}</td>
                                    <td>{item.multiplayer}</td>
                                    <td>{item.platform}</td>
                                    <td>{item.release}</td>
                                    <td>
                                        <img src={item.image_url} alt={item.name} width="50" />
                                    </td>
                                    <td>
                                        <button onClick={handleDelete} value={item.id}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </>
    )
}

export default Game