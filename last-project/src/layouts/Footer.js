import React from "react"

const Footer = () => {
    return (
        <footer>
            <h5 style={{ textAlign: "center" }}>copyright &copy; 2020 by DwiBagus</h5>
        </footer>
    )
}

export default Footer