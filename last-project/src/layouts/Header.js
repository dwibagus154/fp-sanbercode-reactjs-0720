import React, { useContext } from "react"
import { Link } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const Header = () => {
    const [user, setUser] = useContext(UserContext)
    const handleLogout = () => {
        setUser(null)
        localStorage.removeItem("user")
    }

    // untuk menu
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [anchorEl1, setAnchorEl1] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClick1 = (event) => {
        setAnchorEl1(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const handleClose1 = () => {
        setAnchorEl1(null);
    };
    // akhir menu

    return (
        <header>
            <img id="logo" src="/images/logo.jpg" width="70px" />
            <nav>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    {user && <Button aria-controls="movie-menu" aria-haspopup="true" onClick={handleClick} style={{ color: "white" }}>
                        Movie Menu
                    </Button>}
                    {user && <Button aria-controls="game-menu" aria-haspopup="true" onClick={handleClick1} style={{ color: "white" }}>
                        Game Menu
                    </Button>}
                    {user === null && <li><Link to="/login">Login </Link></li>}
                    {user === null && <li><Link to="/register">Register </Link></li>}
                    {user && <li><a style={{ cursor: "pointer" }} onClick={handleLogout}>Logout </a></li>}
                </ul>
            </nav>
            {/* untuk menu */}
            <Menu
                id="movie-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem>{user && <li><Link to="/movies">Movie</Link></li>}</MenuItem>
                <MenuItem>{user && <li><Link to="/movieList">Movie List</Link></li>}</MenuItem>
                <MenuItem>{user && <li><Link to="/movieCreate">Movie Create</Link></li>}</MenuItem>
            </Menu>
            {/* untuk Game */}
            <Menu
                id="game-menu"
                anchorEl={anchorEl1}
                keepMounted
                open={Boolean(anchorEl1)}
                onClose={handleClose1}
            >
                <MenuItem>{user && <li><Link to="/games">Game</Link></li>}</MenuItem>
                <MenuItem>{user && <li><Link to="/gamesList">Game List</Link></li>}</MenuItem>
                <MenuItem>{user && <li><Link to="/gamesCreate">Game Create</Link></li>}</MenuItem>
            </Menu>
        </header>


    )
}

export default Header