import React, { useContext } from "react"
import {
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import Login from "../Pages/login"
import Register from "../Pages/register"
import { UserContext } from "../context/UserContext"
import Game from "../Pages/game"
import Movies from "../Pages/movies"
import GameCreate from "../Pages/gameCreate"
import GameList from "../Pages/gameList"
import MovieCreate from "../Pages/movieCreate"
import MovieList from "../Pages/movieList"
import Forgot from "../Pages/forgotPass"
import Home from "../Pages/Home"



const Section = () => {

    const [user] = useContext(UserContext);

    const PrivateRoute = ({ user, ...props }) => {
        if (user) {
            return <Route {...props} />;
        } else {
            return <Redirect to="/login" />;
        }
    };

    const LoginRoute = ({ user, ...props }) =>
        user ? <Redirect to="/" /> : <Route {...props} />;



    return (
        <section >
            <Switch>
                <Route exact path="/" user={user} component={Home} />
                <LoginRoute exact path="/login" user={user} component={Login} />
                <LoginRoute exact path="/register" user={user} component={Register} />
                <LoginRoute exact path="/forgot" user={user} component={Forgot} />
                <PrivateRoute exact path="/movies" user={user} component={Movies} />
                <PrivateRoute exact path="/movieList" user={user} component={MovieList} />
                <PrivateRoute exact path="/movieCreate" user={user} component={MovieCreate} />
                <PrivateRoute exact path="/games" user={user} component={Game} />
                <PrivateRoute exact path="/gamesList" user={user} component={GameList} />
                <PrivateRoute exact path="/gamesCreate" user={user} component={GameCreate} />
            </Switch>
        </section>
    )
}

export default Section